import csv
from tkinter import Tk, filedialog, messagebox, Menu, Label, Entry, Button, PhotoImage
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

data = None  # Глобальная переменная для хранения данных из файла

def inst():
    messagebox.showinfo("Инструкция", "1. Загрузите файл\n" 
                                      "2. Введите названия осей\n"
                                      "3. Нажмите построить график\n"
                                      "4. График построен")
def info():
    messagebox.showinfo("Информация", "Выполнила Шипунова Анастасии 5.205-2")

def open_and_read_file():
    filename = filedialog.askopenfilename(filetypes=[("CSV Files", "*.csv")])
    if filename:
        with open(filename, 'r') as file:
            reader = csv.DictReader(file, delimiter=';')
            global data
            data = list(reader)
            messagebox.showinfo("Успех", "Файл успешно загружен.")
    else:
        messagebox.showwarning("Ошибка", "Файл не выбран.")

def plot_graph():
    if not data:
        messagebox.showwarning("Ошибка", "Файл не загружен.")
        return

    x_column = x_entry.get()
    y_column = y_entry.get()

    if x_column and y_column:
        if x_column not in data[0] or y_column not in data[0]:
            messagebox.showwarning("Ошибка", "Таких столбцов нет.")
            return

        x_values = []
        y_values = []

        for row in data:
            x_values.append(float(row[x_column]))
            y_values.append(float(row[y_column]))

        # Создаем фигуру Matplotlib
        fig = Figure(figsize=(6, 4), dpi=100)
        plot = fig.add_subplot(111)
        plot.plot(x_values, y_values)
        plot.set_xlabel(x_column)
        plot.set_ylabel(y_column)
        plot.set_title("График")

        # Удаляем предыдущий холст, если он существует
        if hasattr(win, 'canvas'):
            win.canvas.get_tk_widget().pack_forget()

        # Создаем виджет холста Tkinter для отображения графика
        canvas = FigureCanvasTkAgg(fig, master=win)
        canvas.draw()

        # Размещаем холст на главном окне
        canvas.get_tk_widget().pack()

        # Сохраняем ссылку на холст в главном окне
        win.canvas = canvas
    else:
        messagebox.showwarning("Ошибка", "Введите названия столбцов для осей X и Y.")

win = Tk()
win.title('Построение графиков из файла CSV')
win.geometry("700x550+400+100")
photo = PhotoImage(file = '422.png' )
win.iconphoto(False, photo)

main_menu = Menu(win)
main_menu.add_cascade(label="Загрузить файл", command=open_and_read_file)
main_menu.add_cascade(label="Инструкция", command=inst)
main_menu.add_cascade(label="Информация", command=info)

win.config(menu=main_menu)

label_x = Label(win, text="Название столбца для оси X:")
label_x.pack()

x_entry = Entry(win)
x_entry.pack()

label_y = Label(win, text="Название столбца для оси Y:")
label_y.pack()

y_entry = Entry(win)
y_entry.pack()

plot_button = Button(win, text="Построить график", command=plot_graph)
plot_button.pack()

win.mainloop()